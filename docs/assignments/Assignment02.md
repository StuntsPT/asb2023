# Assignment 02

## *High Throughput Sequencing at your fingertips*

### Groups:

* Work in **pairs**
   * Alternatively, you can work alone
   * No groups of 3 people are allowed


### Deadline:

June 27th 2023


### Delivery format:

* English or Portuguese;
* PDF;
* Shell scripts, python scripts or whatever alternate automation method you see fit;


### Delivery method:

E-mail the report PDF;
Online `git` repository for code;


### Your Task:

For this assignment you will have to:

* Search the bibliography for a research paper that performs population genetics analyses;
    * This paper **must** use RAD-Seq or GBS (or one of their variants);
        * *Hint*: Search "Google Scholar" for terms like "GBS", or "RAD-Seq" combined with a species name you like
* Understand the proposed biological problem;
* Obtain the *raw reads* **OR** the *demultiplexed* reads used in the original paper;
    * Where to obtain this data should be stated in the paper, usually in the form of accession numbers in the main text;
* Reduce the dataset to something you consider manageable (optional):
    * If your CPU is an "old potato" and you have low amounts of RAM available (≤ 8GB), don't go above 4-6M reads;
    * If your CPU is "mid-range" and you have a decent amount of RAM available (≤ 16GB), don't go above 10M reads;
    * If you are lucky enough to have a "high-end" CPU (anything above a Ryzen 7 2700) available and have a generous amount of RAM available (≥ 20GB) you can probably analyse the entire dataset!;
        * No need for randomization strategies, you can just use the first *N* reads from the files;
        * *Hint*: `head` is your friend when reducing your dataset;
* Reanalyse the obtained data using both a PCA and admixture plots;
    * Compare your results with those in the original paper;
* Make sure your analyses are reproducible;
* Write a *short* report on what you did.


### In detail:

#### Cover

According to EST's rules. Must include *at least*:

* Title
* EST Barreiro logo
* Date
* Student ID
* Curricular Unit name


#### Introduction

A short (1-2 pages) section describing:
  
* The original biological problem;
* The original used bioinformatics pipeline;
    * As a figure;
* The conclusions from the original paper;


#### Objectives

* A single paragraph of context;
* The main goal of your work as a bullet point;
    * Technical tasks needed for your main goal as sub-bullet points;
    * Associate each task with a skill set;


#### Materials & Methods

A section where you **detail** how your analyses were performed:

* Which methods were used **for each task**;
* Which software (don't forget to include version number) was used;
* Where can the used scripts be found (there is no way to work around it this time. You **must** make the code available);
    * Create a [github](https://github.com) or [gitlab](https://gitlab.com) repository for this;
    * You are not restricted to these two services, but a `git` repository is a requirement;
* Your pipeline should also be presented as a figure.


#### Results

Produce plots similar to those in the original paper and present them here (eventually alongside the original ones).

* Are your plots *compatible* with those from the original paper?
    * This is a simple "yes" or "no" question;
    * Critically analyzing the "why" will help you find any mistakes in the process (it is highly unlikely you succeed at first try, don't feel bad about it);


#### Discussion

Expand on the results' answer here.

* Explore the differences between your results and the original ones;
* Interpret any propose hypotheses to explain any differences;
* How could eventual discrepancies be mitigated?


#### Author contributions

Note down which tasks each group member was responsible for. Which skills did each member felt they improved?


#### References

Don't just present a list of consulted material. That simply won't suffice anymore.
For this work (and from now on) you must present the citations in text in addition to the reference list. This time the mandatory style is "APA (American Psychological Association) 7th Edition".
See how it is done in Akihito et al. 2016, the paper from your previous assignment or the paper you are currently analysing.
It is highly recommended that you use reference managing software, like [Zotero](https://www.zotero.org/), [Mendeley](https://www.mendeley.com/download-desktop-new/), or [Endnote](https://endnote.com/) (But there are other alternatives).


### Hints:

* The tougher part of this task will be to find a suitable paper with an associated and available dataset. If you thought the last one was hard, just wait until you try this one!
* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use;
* Any used scripts (even if they are only for keeping track of commands), **must** be placed in an online repository like [github](https://github.com) or [gitlab](https://gitlab.com);
* This type of data might take anywhere from several minutes to 24h to analyse (depending on CPU speed and dataset size). **Plan in advance**;
* Did I mention references before? **DO NOT FORGET TO INCLUDE REFERENCES!!** These have, of course, to be included in the main text, like you see in every research paper;
    * Do not cite only the paper you tried to reproduce. Knowledge comes from other papers too. Cite them as you gather information from them;
    * When making citations for software, cite the respective paper (when available) instead of the website;
* This analysis is not as straightforward as the ones from the tutorials. **There are bound to be problems and other analyses issues**. Solve them as best you can;
* Do not hesitate to ask any questions you may have via email;


## Be at your 'top game' for this one. I'm counting on you.
