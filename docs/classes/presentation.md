### classes[0] = "Presentation"

#### Análise de Sequências Biológicas 2022-2023

![Logo EST](presentation_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Practical info

* &shy;<!-- .element: class="fragment" --> Schedule: Tuesdays - 11:00 to 13:00; Thursdays - 9:30 to 11:30 **Room 1.10**
* &shy;<!-- .element: class="fragment" --> Questions? f.pina.martins@estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" --> [Moodle](https://moodle.ips.pt/2223/course/view.php?id=431)
* &shy;<!-- .element: class="fragment" --> Office hours: TBD

---

### Avaliação

* &shy;<!-- .element: class="fragment" --><font color="orange">Contínua</font> **OU** <font color="deeppink">exame 1ª e/ou 2ª época</font>
* &shy;<!-- .element: class="fragment" --><font color="orange">Avaliação contínua:</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">30% - Contexto sala de aula <font color="red">(3 componentes)</font>:</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">10% - "Homework"</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">15% - "Journal Club"</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Participação/Atitude/interesse</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">35% - 1º trabalho</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">35% - 2º trabalho</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Sem notas mínimas</font>
* &shy;<!-- .element: class="fragment" -->Obter aprovação da UC em <font color="orange">avaliação contínua</font> impede accesso a <font color="deeppink">exame de 1ª época</font></font>

---

### A word of caution

---

### What are "biological sequences"?

* &shy;<!-- .element: class="fragment" -->A biological sequence is a single, continuous molecule of nucleic acid or protein.
* &shy;<!-- .element: class="fragment" -->Can represent multiple types of molecules
* &shy;<!-- .element: class="fragment" -->Can represent multiple types of data

|||

### Such as?

* &shy;<!-- .element: class="fragment" -->By molecule type:
  * &shy;<!-- .element: class="fragment" --><font color="orange">DNA</font>
  * &shy;<!-- .element: class="fragment" --><font color="green">RNA</font>
  * &shy;<!-- .element: class="fragment" --><font color="cyan">Protein</font>
* &shy;<!-- .element: class="fragment" -->By data structure type:
  * &shy;<!-- .element: class="fragment" --><font color="purple">Single sequence</font>
  * &shy;<!-- .element: class="fragment" --><font color="yellow">Multiple sequences</font>
  * &shy;<!-- .element: class="fragment" -->Can be organized as:
    * &shy;<!-- .element: class="fragment" --><font color="#FFCC66">Independent sequences</font>
    * &shy;<!-- .element: class="fragment" --><font color="navy">Alignment</font>
    * &shy;<!-- .element: class="fragment" --><font color="grey">Mapping</font>
    * &shy;<!-- .element: class="fragment" --><font color="deeppink">Assembly</font>

---

### Sooo... Why do we analyse them?

* Phylogenetic trees <!-- .element: class="fragment" data-fragment-index="1" -->
* Population genetics/genomics <!-- .element: class="fragment" data-fragment-index="2" -->
* Organism identification <!-- .element: class="fragment" data-fragment-index="3" -->
* Natural selection detection <!-- .element: class="fragment" data-fragment-index="4" -->
* Finding gene variants <!-- .element: class="fragment" data-fragment-index="5" -->
* Disease prediction <!-- .element: class="fragment" data-fragment-index="6" -->
* Association studies <!-- .element: class="fragment" data-fragment-index="7" -->
* etc.. <!-- .element: class="fragment" data-fragment-index="8" -->

---

### Overview

* Setting up a working environment <!-- .element: class="fragment" data-fragment-index="1" -->
* Sequence concepts revision <!-- .element: class="fragment" data-fragment-index="2" -->
* Sequence formats <!-- .element: class="fragment" data-fragment-index="3" -->
* Database access <!-- .element: class="fragment" data-fragment-index="4" -->
* Sequence alignments <!-- .element: class="fragment" data-fragment-index="5" -->
* An intro to Phylogenetics <!-- .element: class="fragment" data-fragment-index="6" -->
* High Throughput Sequencing data analyses <!-- .element: class="fragment" data-fragment-index="7" -->

---

### References

* [NCBI Toolbox](https://www.ncbi.nlm.nih.gov/IEB/ToolBox/SDKDOCS/BIOSEQ.HTML)
